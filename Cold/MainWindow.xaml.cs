﻿using OpenHardwareMonitor.Hardware;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace Cold
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {

        UpdateVisitor updateVisitor;
        Computer computer;

        NotifyIcon notifyIcon;

        public MainWindow()
        {
            InitializeComponent();

            this.Topmost = true;

            SystemTrayParameter pars = new SystemTrayParameter("cold.ico", "Cold", "", 0, notifyIcon_MouseDoubleClick);
            this.notifyIcon = WPFSystemTray.SetSystemTray(pars, GetList());


            this.Left = (SystemParameters.WorkArea.Width / 2) - (this.Width / 2);
            this.Top = 10;



            updateVisitor = new UpdateVisitor();
            computer = new Computer();

            computer.MainboardEnabled = true;
            computer.FanControllerEnabled = true;
            computer.CPUEnabled = true;
            computer.GPUEnabled = true;
            computer.RAMEnabled = true;
            computer.HDDEnabled = true;

            computer.Open();


            DispatcherTimer dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += OnTimedEvent;
            dispatcherTimer.Interval = TimeSpan.FromMilliseconds(1000);
            dispatcherTimer.Start();

        }

        SystemTrayMenu monitorMenu;
        private List<SystemTrayMenu> GetList()
        {
            monitorMenu = new SystemTrayMenu() { Txt = "Hardware", Check = true, Click = mainWin_Click };

            List<SystemTrayMenu> ls = new List<SystemTrayMenu>();
            ls.Add(monitorMenu);
            ls.Add(new SystemTrayMenu() { Txt = "exit", Click = exit_Click });
            return ls;
        }

        
        void mainWin_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = (ToolStripMenuItem)notifyIcon.ContextMenuStrip.Items[0];
            Console.WriteLine(item.Checked);

            if (item.Checked)
            {
                this.Hide();
                this.Topmost = false;
                item.Checked = false;
            }
            else
            {
                item.Checked = true;
                this.Show();
                this.Topmost = true;

            }
        }

        void exit_Click(object sender, EventArgs e)
        {

            computer.Close();
            this.Close();
            System.Windows.Application.Current.Shutdown();
        }


        void notifyIcon_MouseDoubleClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {

        }


        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                this.DragMove();
            }
            catch { }
        }


        public void OnTimedEvent(object sender, EventArgs e)
        {

            computer.Accept(updateVisitor);

            try
            {
                for (int i = 0; i < computer.Hardware.Length; i++)
                {

                    if (computer.Hardware[i].HardwareType == HardwareType.CPU)
                    {
                        for (int j = 0; j < computer.Hardware[i].Sensors.Length; j++)
                        {
                            if (computer.Hardware[i].Sensors[j].SensorType == SensorType.Temperature)
                            {
                                float CPUTem = (float)computer.Hardware[i].Sensors[j].Value;

                                if (CPUTem > 60) this.CPUTem.Foreground = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                                else this.CPUTem.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));


                                this.CPUTem.Content = CPUTem.ToString() + "℃";

                                Console.WriteLine("CPU " + computer.Hardware[i].Sensors[j].Value + "℃");
                            }

                            if (computer.Hardware[i].Sensors[j].SensorType == SensorType.Load)
                            {
                                float CPULoad = (float)computer.Hardware[i].Sensors[j].Value;

                                if (CPULoad > 60) this.CPULoad.Foreground = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                                else this.CPULoad.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));


                                this.CPULoad.Content = Math.Round(CPULoad, 1).ToString() + "%";

                                Console.WriteLine("CPU Load" + CPULoad + "%");

                            }
                        }
                    }


                    if (computer.Hardware[i].HardwareType == HardwareType.GpuNvidia)
                    {
                        for (int j = 0; j < computer.Hardware[i].Sensors.Length; j++)
                        {
                            if (computer.Hardware[i].Sensors[j].SensorType == SensorType.Temperature)
                            {

                                float GPUTem = (float)computer.Hardware[i].Sensors[j].Value;

                                if (GPUTem > 80) this.GPUTem.Foreground = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                                else this.GPUTem.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));

                                this.GPUTem.Content =  GPUTem.ToString() + " ℃";


                                Console.WriteLine("NVIDIA GPU " + computer.Hardware[i].Sensors[j].Value + "℃");
                            }

                            if (computer.Hardware[i].Sensors[j].SensorType == SensorType.Load)
                            {
                                float GPULoad = (float)computer.Hardware[i].Sensors[j].Value;

                                if (GPULoad > 80) this.GPULoad.Foreground = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                                else this.GPULoad.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));

                                this.GPULoad.Content = Math.Round(GPULoad, 1).ToString() + " %";

                                Console.WriteLine("NVIDIA GPU " + GPULoad + "%");

                            }

                            if (computer.Hardware[i].Sensors[j].SensorType == SensorType.Fan)
                            {
                                float GPUFans = (float)computer.Hardware[i].Sensors[j].Value;

                                this.GPUFan.Content = GPUFans.ToString() + " RPM";

                                Console.WriteLine("NVIDIA GPU " + GPUFans + "RPM");

                            }
                        }
                    }


                    if (computer.Hardware[i].HardwareType == HardwareType.GpuAti)
                    {
                        for (int j = 0; j < computer.Hardware[i].Sensors.Length; j++)
                        {
                            if (computer.Hardware[i].Sensors[j].SensorType == SensorType.Temperature)
                            {


                                float GPUTem = (float)computer.Hardware[i].Sensors[j].Value;

                                if (GPUTem > 80) this.GPUTem.Foreground = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                                else this.GPUTem.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));

                                this.GPUTem.Content = GPUTem.ToString() + " ℃";


                                Console.WriteLine("ATI GPU " + computer.Hardware[i].Sensors[j].Value + "℃");

                            }

                            if (computer.Hardware[i].Sensors[j].SensorType == SensorType.Load)
                            {
                                float GPULoad = (float)computer.Hardware[i].Sensors[j].Value;

                                if (GPULoad > 80) this.GPULoad.Foreground = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                                else this.GPULoad.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));

                                this.GPULoad.Content = Math.Round(GPULoad, 1).ToString() + " %";

                                Console.WriteLine("ATI GPU " + GPULoad + "%");

                            }

                            if (computer.Hardware[i].Sensors[j].SensorType == SensorType.Fan)
                            {
                                float GPUFans = (float)computer.Hardware[i].Sensors[j].Value;

                                this.GPUFan.Content = GPUFans.ToString() + " RPM";

                                Console.WriteLine("ATI GPU " + GPUFans + "RPM");

                            }
                        }
                    }


                    if (computer.Hardware[i].HardwareType == HardwareType.RAM)
                    {
                        for (int j = 0; j < computer.Hardware[i].Sensors.Length; j++)
                        {
                            if (computer.Hardware[i].Sensors[j].SensorType == SensorType.Load)
                            {

                                double RAMsyl = (double)computer.Hardware[i].Sensors[j].Value;

                                if (RAMsyl > 90) this.RAMLoad.Foreground = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                                else this.RAMLoad.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));

                                this.RAMLoad.Content = Math.Round(RAMsyl, 1).ToString() + " %";


                                Console.WriteLine("RAM " + computer.Hardware[i].Sensors[j].Value + "%");
                            }

                        }
                    }


                }
            }
            catch { }
        }

    }


    public class UpdateVisitor : IVisitor
    {
        public void VisitComputer(IComputer computer)
        {
            computer.Traverse(this);
        }

        public void VisitHardware(IHardware hardware)
        {
            hardware.Update();
            foreach (IHardware subHardware in hardware.SubHardware)
                subHardware.Accept(this);
        }

        public void VisitSensor(ISensor sensor) { }

        public void VisitParameter(IParameter parameter) { }

    }
}
